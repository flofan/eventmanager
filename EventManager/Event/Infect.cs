﻿using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.API;
using System.Timers;
using Smod2.EventSystem.Events;

namespace EventManager.Event
{
    public class Infect : IEventHandlerSetRole, IEventHandlerCheckEscape, IEventHandlerPlayerHurt, IEventHandlerPlayerDie, IEventHandlerSetSCPConfig, IEventHandlerDecideTeamRespawnQueue, IEventHandlerPlayerJoin

    {
        private Main main;

        public Infect(Main main)
        {
            this.main = main;
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                if (ev.TeamRole.Role == Role.CLASSD) ev.Items.Add(ItemType.ZONE_MANAGER_KEYCARD);
            }
        }

        public void OnCheckEscape(PlayerCheckEscapeEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                if (ev.Player.TeamRole.Role == Role.CLASSD || ev.Player.TeamRole.Role == Role.SCIENTIST)
                {
                    ev.AllowEscape = true;
                    ev.ChangeRole = Role.NTF_COMMANDER;
                }
            }
        }

        public void OnPlayerHurt(PlayerHurtEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                Player v = ev.Player;
                Player k = ev.Attacker;
                if (k.TeamRole.Role == Role.SCP_049_2 || k.TeamRole.Role == Role.SCP_049)
                {
                    Vector posv = v.GetPosition();
                    Timer t = new Timer(1000);
                    t.Enabled = true;
                    t.AutoReset = false;
                    t.Start();
                    t.Elapsed += delegate
                    {
                        v.ChangeRole(Role.SCP_049_2, true, true, true, true);
                        v.Teleport(posv);
                        v.SetHealth(400);
                    };
                }
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                Player v = ev.Player;
                Player k = ev.Killer;
                if (k.TeamRole.Role == Role.SCP_049_2 || k.TeamRole.Role == Role.SCP_049)
                {
                    Vector posv = v.GetPosition();
                    Timer t = new Timer(1000);
                    t.Enabled = true;
                    t.AutoReset = false;
                    t.Start();
                    t.Elapsed += delegate
                    {
                        v.ChangeRole(Role.SCP_049_2, true, true, true, true);
                        v.Teleport(posv);
                        v.SetHealth(400);
                    };
                }
            }
        }

        public void OnSetSCPConfig(SetSCPConfigEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                ev.Ban049 = false;

                ev.Ban079 = true;
                ev.Ban096 = true;
                ev.Ban106 = true;
                ev.Ban173 = true;
                ev.Ban939_53 = true;
                ev.Ban939_89 = true;
            }
        }

        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                ev.Teams = new Smod2.API.Team[] { Smod2.API.Team.CLASSD, Smod2.API.Team.SCP, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD };
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "infect")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Un mode infecter est là.", true);
            }
        }
    }
}