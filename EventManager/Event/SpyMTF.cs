﻿using System.Collections.Generic;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;
using Smod2.API;
using System;
using Smod2;
using System.Timers;
using System.Linq;

namespace EventManager.Event
{
    internal class SpyMTF : IEventHandlerTeamRespawn, IEventHandlerCheckRoundEnd, IEventHandlerPlayerHurt, IEventHandlerPlayerDie, IEventHandlerRoundEnd, IEventHandlerDisconnect, IEventHandlerPlayerJoin
    {
        private Main main;
        private Plugin plugin;

        public SpyMTF(Main main)
        {
            this.main = main;
        }

        public SpyMTF(Plugin plugin)
        {
            this.plugin = plugin;
        }

        public static List<string> spylist = new List<string>();

        public void OnCheckRoundEnd(CheckRoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                if (ev.Status == ROUND_END_STATUS.MTF_VICTORY && spylist.Count >= 1)
                {
                    ev.Status = ROUND_END_STATUS.ON_GOING;
                }

                if (ev.Round.Stats.CiAlive == 0 && ev.Round.Stats.SCPAlive >= 1 && ev.Round.Stats.ScientistsAlive == 0 && ev.Round.Stats.NTFAlive == 1 && ev.Round.Stats.ClassDAlive == 0 && spylist.Count >= 1)
                {
                    ev.Status = ROUND_END_STATUS.SCP_CI_VICTORY;
                }

                if (ev.Round.Stats.CiAlive >= 1 && ev.Round.Stats.SCPAlive >= 1 && ev.Round.Stats.ScientistsAlive == 0 && ev.Round.Stats.NTFAlive == 1 && ev.Round.Stats.ClassDAlive == 0 && spylist.Count >= 1)
                {
                    ev.Status = ROUND_END_STATUS.SCP_CI_VICTORY;
                }

                if (ev.Round.Stats.CiAlive >= 1 && ev.Round.Stats.SCPAlive == 0 && ev.Round.Stats.ScientistsAlive == 0 && ev.Round.Stats.NTFAlive == 1 && ev.Round.Stats.ClassDAlive >= 0 && spylist.Count >= 1)
                {
                    ev.Status = ROUND_END_STATUS.CI_VICTORY;
                }
            }

        }

        public void OnPlayerHurt(PlayerHurtEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                if (spylist.Contains(ev.Attacker.SteamId) && ev.Player.TeamRole.Team == Smod2.API.Team.NINETAILFOX || ev.Player.TeamRole.Team == Smod2.API.Team.SCIENTIST)
                {
                    ev.Player.PersonalBroadcast(10, "Un espion vous a tiré dessus", true);
                }
            }

        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {

                if (ev.SpawnChaos == false)
                {
                    if (!(spylist.Count >= 1))
                    {
                        Timer t = new Timer(100);
                        t.AutoReset = false;
                        t.Start();
                        t.Elapsed += delegate
                        {
                            List<Player> mtflist = ev.PlayerList;

                            int i = new Random().Next(0, mtflist.Count);
                            spylist.Add(mtflist[i].SteamId);

                            foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                            {
                                if (spylist[0].Contains(p.SteamId))
                                {
                                    p.PersonalClearBroadcasts();
                                    p.PersonalBroadcast(10, "<color=red><b></b>Spy MTF</color>\n Vous êtes un espion chez les IC ! \nFaites attention à ne pas vous faire avoir !", true);
                                    p.ChangeRole(Role.NTF_LIEUTENANT);
                                    p.GiveItem(ItemType.FLASHLIGHT);
                                }
                            }

                            mtflist.Clear();
                        };
                    }
                }
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                if (ev.Killer.TeamRole.Role == Role.CHAOS_INSURGENCY && spylist.Contains(ev.Player.SteamId))
                {
                    ev.Killer.SendConsoleMessage("You have killed a CI SPY ! Take care next time.", "red");
                }
                if (spylist.Contains(ev.Player.SteamId))
                {
                    spylist.Remove(ev.Player.SteamId);
                    PluginManager.Manager.Server.Map.Broadcast(10, "<color=#10a000>Chaos Insurgency</color> spy has been <color=red>neutralised</color> !", true);
                }
            }

        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                spylist.Clear();
            }
        }

        public void OnDisconnect(DisconnectEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                if (spylist.Count >= 1)
                {
                    foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                    {
                        if (p.SteamId == spylist[0])
                        {
                            return;
                        }
                    }
                    spylist.RemoveAt(0);
                }
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "spymtf")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Un espion chez les MTF peut se \njoindre à la fête lors d'un respawn", true);
            }
        }
    }
}