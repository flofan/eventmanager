﻿using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem;
using Smod2;
using Smod2.API;
using System.Timers;

namespace EventManager.Event
{
    public class PeanutRun : IEventHandlerRoundStart, IEventHandlerWarheadDetonate, IEventHandlerCheckRoundEnd, IEventHandlerDoorAccess, IEventHandlerPlayerJoin
    {
        private Main main;
        private bool roundlock = false;

        public PeanutRun(Main main)
        {
            this.main = main;
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "peanutrun")
            {

                roundlock = true;
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    p.ChangeRole(Smod2.API.Role.SCP_173);
                }

                Timer td = new Timer(1000);
                td.AutoReset = false;
                td.Enabled = true;
                td.Elapsed += delegate
                {
                    foreach (Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors())
                    {
                        if (d.Name == "GATE_A" || d.Name == "GATE_B")
                        {
                            d.Locked = false;
                            d.BlockAfterWarheadDetonation = false;
                            d.DontOpenOnWarhead = false;
                            d.Open = true;
                        }
                    }
                };

                Timer t = new Timer(120000);
                t.AutoReset = false;
                t.Enabled = true;
                t.Start();
                t.Elapsed += delegate
                {
                    PluginManager.Manager.Server.Map.Broadcast(10, "<color=red><b>PeanutRUN</b></color>\n NUKE ON ! Bonne chance ;3" ,true);

                    PluginManager.Manager.CommandManager.CallCommand(PluginManager.Manager.Server, "nuke", new string[] { "on" });
                    PluginManager.Manager.CommandManager.CallCommand(PluginManager.Manager.Server, "nuke", new string[] { "start" });
                    PluginManager.Manager.CommandManager.CallCommand(PluginManager.Manager.Server, "nuke", new string[] { "lock" });
                };
            }
        }

        public void OnCheckRoundEnd(CheckRoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "peanutrun")
            {
                if (roundlock == true)
                {
                    ev.Status = ROUND_END_STATUS.ON_GOING;
                }
            }
        }
        public void OnDetonate()
        {
            if (EventManager.API.EventAPI.eventNameOn == "peanutrun")
            {
                roundlock = false;
            }
        }

        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "peanutrun")
            {
                ev.Allow = true;
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "peanutrun")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Tu es une cacahuète qui doit sortir avant\n que la nuke explose. \nActivation dans 2 minutes.", true);
            }
        }
    }
}