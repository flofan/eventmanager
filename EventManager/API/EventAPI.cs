﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Smod2;
using Smod2.API;

namespace EventManager.API
{
    public static class EventAPI
    {
        public static List<string> eventList = new List<string>();
        public static string eventNameOn = null;

        public static bool isRoundStarted = false;
        public static bool isNukeExplode = false;
        public static bool isDecontON = false;

        public static bool CheckNameEvent(string name)
        {
            return eventList.Contains(name);
        }

        public static void AddEvent(string name)
        {
            if (!eventList.Contains(name))
            {
                eventList.Add(name);
            }
        }

        public static void ActivateEvent(string name, bool force = false)
        {
            if (eventList.Contains(name))
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    p.PersonalBroadcast(11, "<color=aqua><b>EVENT</b></color>\n Changement d'Event vers " + name + ". \n Restart dans <color=green>10</color> secondes.", true);
                }
                Timer t = new Timer(11000);
                t.AutoReset = false;
                t.Enabled = true;
                t.Start();
                t.Elapsed += delegate
                {
                    eventNameOn = name;
                    PluginManager.Manager.Server.Round.RestartRound();
                };
            }
        }

        public static void DisableEvent()
        {
                Timer t = new Timer(11000);
                t.AutoReset = false;
                t.Enabled = true;
                t.Start();
                t.Elapsed += delegate
                {
                    eventNameOn = null;
                    PluginManager.Manager.Server.Round.RestartRound();
                };
        }
    }
}
