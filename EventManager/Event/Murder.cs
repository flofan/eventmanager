﻿using System;
using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;

namespace EventManager.Event
{
    internal class Murder : IEventHandlerRoundStart, IEventHandlerPlayerDie, IEventHandlerPlayerHurt, IEventHandlerPlayerJoin, IEventHandlerTeamRespawn, IEventHandlerDecideTeamRespawnQueue, IEventHandlerShoot, IEventHandlerCheckRoundEnd
    {
        private Main main;

        public string murder = "mort";
        public string detec = "mort";

        public Murder(Main main)
        {
            this.main = main;
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                foreach (Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors())
                {
                    if (d.Name == "CHECKPOINT_LCZ_A" || d.Name == "CHECKPOINT_LCZ_B")
                    {
                        d.Locked = true;
                    }
                }

                int murdr = new Random().Next(0, PluginManager.Manager.Server.GetPlayers().Count);
                int detecr = new Random().Next(0, PluginManager.Manager.Server.GetPlayers().Count);

                if (murdr == detecr)
                {
                    detecr++;
                }

                murder = PluginManager.Manager.Server.GetPlayers()[murdr].SteamId;
                detec = PluginManager.Manager.Server.GetPlayers()[detecr].SteamId;

                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (p.SteamId == murder)
                    {
                        p.PersonalBroadcast(10, "<color=aqua><b>MURDER</b></color> \n Vous êtes un <color=red>Murder</color> ! \nTuez tout le monde sans vous faire remarquer.", true);
                        p.GetInventory().Clear();
                        p.GiveItem(ItemType.USP);
                    }
                    if (p.SteamId == detec)
                    {
                        p.PersonalBroadcast(10, "<color=aqua><b>MURDER</b></color> \n Vous êtes un <color=green>Détective</color> ! \nTuez le Murder.", true);
                        p.GiveItem(ItemType.USP);
                        p.GetInventory().Clear();
                    }
                    if (p.SteamId != detec && p.SteamId != murder)
                    {
                        p.PersonalBroadcast(10, "<color=aqua><b>MURDER</b></color> \n Vous êtes un <color=green>Inocent</color> ! \nCourez pour votre vie.", true);
                        p.GetInventory().Clear();
                    }
                }
            }

        }

        public void OnCheckRoundEnd(CheckRoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                if (murder != "mort" && ev.Round.Stats.ClassDAlive >= 2)
                {
                    ev.Status = ROUND_END_STATUS.ON_GOING;
                }

                if (murder != "mort" && ev.Round.Stats.ClassDAlive == 1)
                {
                    ev.Status = ROUND_END_STATUS.SCP_VICTORY;
                }

                if (murder == "mort")
                {
                    ev.Status = ROUND_END_STATUS.MTF_VICTORY;
                }
            }

        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    p.PersonalClearBroadcasts();
                }
                if (ev.Player.SteamId == murder)
                {
                    murder = "mort";
                    foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                    {
                        p.PersonalClearBroadcasts();
                    }
                }

                if (ev.Player.SteamId == detec)
                {
                    detec = "mort";
                    foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                    {
                        p.PersonalClearBroadcasts();
                    }
                    PluginManager.Manager.Server.Map.Broadcast(10, "<color=aqua><b>MURDER</b></color> \n Le détective est mort.", true);
                }
            }
        }

        public void OnPlayerHurt(PlayerHurtEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                if (ev.Attacker.SteamId != murder && ev.Player.SteamId != murder)
                {
                    ev.Attacker.Kill(DamageType.WALL);
                }


                if (ev.DamageType == DamageType.USP)
                {
                    ev.Damage = 100000;
                }
                else
                {
                    ev.Damage = 0;
                }
            }

        }

        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                ev.Teams = new Smod2.API.Team[] { Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD, Smod2.API.Team.CLASSD };
            }
        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                foreach (Player p in ev.PlayerList)
                {
                    p.ChangeRole(Role.SPECTATOR);
                }
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Un murder parmit les classes Ds. Qui va finir en charcuterie ?\n La question dans quelques instants", true);
        }

        public void OnShoot(PlayerShootEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "murder")
            {
                ev.Player.SetAmmo(AmmoType.DROPPED_5, 100);
                ev.Player.SetAmmo(AmmoType.DROPPED_7, 100);
                ev.Player.SetAmmo(AmmoType.DROPPED_9, 100);
            }
        }
    }
}