﻿using Smod2.EventHandlers;
using Smod2.Events;
using Smod2;
using Smod2.API;
using System.Timers;
using Smod2.EventSystem.Events;

namespace EventManager.Event
{
    public class CivsMTF : IEventHandlerRoundStart, IEventHandlerSetRole, IEventHandlerHandcuffed, IEventHandlerDecideTeamRespawnQueue, IEventHandlerPlayerJoin, IEventHandlerWaitingForPlayers
    {
        private Main main;
        public CivsMTF(Main main)
        {
            this.main = main;
        }

        public void OnHandcuffed(PlayerHandcuffedEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {
                ev.Handcuffed = false;
                ev.Player.SetHealth(ev.Player.GetHealth() - 50);
            }
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {
                if(ev.Player.TeamRole.Role == Role.FACILITY_GUARD)
                {
                    ev.Role = Role.NTF_LIEUTENANT;
                }

                if (ev.Player.TeamRole.Role == Role.CHAOS_INSURGENCY)
                {
                    Vector spawnpos = PluginManager.Manager.Server.Map.GetRandomSpawnPoint(Role.CLASSD);
                    ev.Player.Teleport(spawnpos, true);
                }
            }
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {             
                Timer t = new Timer(100);
                t.AutoReset = false;
                t.Enabled = true;
                t.Start();
                t.Elapsed += delegate
                {
                    foreach (Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors())
                    {
                        if (d.Name == "GATE_A" || d.Name == "GATE_B")
                        {
                            d.Locked = false;
                        }
                    }
                };
            }
        }
        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {
                ev.Teams = new Smod2.API.Team[] { Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX, Smod2.API.Team.CHAOS_INSURGENCY, Smod2.API.Team.NINETAILFOX };
            }
                
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Les chaos se battent contre les MTF", true);
            }
        }

        public void OnWaitingForPlayers(WaitingForPlayersEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "civsmtf")
            {
                string serverName = ev.Server.Name + "\n<size=25><color=#ff0000ff>Event en cours : CI vs MTF</color></size>";
            }
        }
    }
}
